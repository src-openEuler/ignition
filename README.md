# ignition

#### 介绍
Ignition is a utility used to manipulate systems during the initramfs.
This includes partitioning disks, formatting partitions, writing files
(regular files, systemd units, etc.), and configuring users. On first
boot, Ignition reads its configuration from a source of truth (remote
URL, network metadata service, hypervisor bridge, etc.) and applies
the configuration.



#### 安装教程

Install ignition rpm package:

yum install ignition

#### 使用说明

ignition 是云底座操作系统NestOS的必需组件，详细说明见下

Odds are good that you don't want to invoke Ignition directly. In fact, it isn't even present in the root filesystem. Take a look at the [Getting Started Guide](https://github.com/coreos/ignition/blob/main/docs/getting-started.md) for details on providing Ignition with a runtime configuration.

#### 参与贡献

master分支使用最新的上游版本，如果检测到上游有最新版本发布，先形成issue后再提交对应PR更新，流程如下。
1.  提交issue
2.  Fork 本仓库
3.  新建 Feat_xxx 分支
4.  提交代码
5.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
